package dCTowerAufzug;
import java.lang.Math;
import java.util.Arrays;
import java.util.Scanner;



public class Aufzugsteuerung {
		
	
	
	//Eigenschaften/Properties
	private int mini = 0;
	private int min = 1;
	private int directionElevetorDown = 1;  
	private int directionElevetorUp = 2;
	private final int max = 55; 
		
	private boolean elevatorCall = true;
	
	private int floorDownCall;
	private int[] elevatorPositionArray = new int[7];
	private int[] workingArray = new int[7];
	private int elevatorNumber;
	
	
	
	//Start des Programms und Input für Testing
	public void run() {
	
		Scanner scanner = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);
		Scanner scanner3 = new Scanner(System.in);


		System.out.println("Bitte geben Sie an, ob Sie nach oben oder unten fahren möchten. 1 für hinauf oder 2 für hinunter.");
		int directionElevator = scanner.nextInt();
		
		if(directionElevator == 1) {
		System.out.println("Sie betreten das Gebäude, geben Sie nun Ihr Zielstockwerk ein.");
		int floorInput = scanner2.nextInt();
		if(floorInput > 0 && floorInput < 56) {
		elevatorNumber = assignElevatorNumber(floorInput);
		} else {
			int floorGenerate = assignFloorNumber();
			elevatorNumber = assignElevatorNumber(floorGenerate);
			floorInput = floorGenerate;
			System.out.println("Die von Ihnen eingegebene Etagennummer gibt es nicht. Es wurde die Etagennummer "+floorInput+" automatisch generiert.");
		}
		System.out.println("Ihre Etagennummer ist "+floorInput);
		System.out.println("Bitte begeben Sie sich zu Aufzugnummer: "+ elevatorNumber);
		
		} else if(directionElevator == 2) {
			System.out.println("Bitte geben Sie ein, in welchem Stockwerk sie sich befinden. (1-55)");
			int currentFloor = scanner3.nextInt();
			if(currentFloor > 0 && currentFloor < 56) {
				runNext(currentFloor);
			} else {
				randomFloorDownCall();// Etagennummer wird generiert
				System.out.println("Die von Ihnen eingegebene Etagennummer gibt es nicht. Es wurde die Etagennummer " +floorDownCall+" automatisch generiert.");
				runNext(floorDownCall);
			}

		}
		
	}

	
	public void runNext(int cFloor) {

			randomFloorDownCall(cFloor);
			randomFloorDown();
			System.out.println("In der Etage Nummer "+floorDownCall +" wurde der Aufzug gerufen.");
			checkNearestElevator();
		
	}
	
	public void checkNearestElevator() {
		
		//Wert von INDEX 0 als int
		int nearestElevator = 0;
		int index = 0;
		int[] workingArrayForExcept = new int[7];
		
		
		//Suchen den niedrigsten wert un die dazugehörige Arrayposition == Aufzugsnummer 1-7
		int nearestElevatorNumber = -1;
		int nearestElevatorForExcept = -1;

		//Zufallsgenerator für die momentanen FLOORS + DIRECTION 
		//200er Werte sind UP und 100er Werte DOWN
		for(int i = 0; i<7; i++) {
			
			workingArrayForExcept[i] = (elevatorPositionArray[i]-floorDownCall);
			
			if(elevatorPositionArray[i]-floorDownCall > 101) {
				workingArray[i]=(elevatorPositionArray[i]-floorDownCall);
				elevatorPositionArray[i]=(elevatorPositionArray[i]-floorDownCall);
			}  else {
				workingArray[i]=0;
				elevatorPositionArray[i]=0;
			}
		}
		

		
		//Niedrigster Wert = nähester Aufzug auf Index 0 zum auslesen
		Arrays.sort(workingArray);
		Arrays.sort(workingArrayForExcept);
		
		try {
			//While-Schleife um Aufzüge die schon vorbei sind auszusortieren.
			while(workingArray[index] == 0) {
				index++;
			}
			
			nearestElevator = workingArray[index];
			
			for(int i = 0; i < 7; i++) {
				if(elevatorPositionArray[i] == nearestElevator)
					nearestElevatorNumber = i+1;
			}
			
			
			System.out.println("Ihr Aufzug ist Nummer: "+nearestElevatorNumber);
			
		} catch(IndexOutOfBoundsException e) {
			
			nearestElevatorForExcept = workingArrayForExcept[0];
			
			for(int i = 0; i < 7; i++) {
				if(elevatorPositionArray[i] == nearestElevatorForExcept)
					nearestElevatorNumber = i+1;
				System.out.println("Der nächste zugewiesene Aufzug ist: " + nearestElevatorNumber);
			}
		}
		
	}
	
	public void randomFloorDownCall() {

		if(elevatorCall) {
		//Fahren von oben nach unten mit bestellung
			int randomFloorCall = (int)(Math.random()*(max-mini+1)+mini);	
			floorDownCall = randomFloorCall;
		}

	}
	
	public void randomFloorDownCall(int callFloor) {
		floorDownCall = callFloor;
	}
	
	public void randomFloorDown() {

		int randomValue;
		//Simulation der Sieben Aufzugspositionen zum Zeitpunkt einer Anforderung für eine fahrt DOWN.
		//200er Werte sind UP und 100er Werte DOWN
		for(int i = 0; i < 7; i++) {
			int randomDirection = (int)(Math.random()*(directionElevetorUp-directionElevetorDown+1)+min)* 100;
		    int randomFloor = (int)(Math.random()*(max-mini+1)+mini);
		    
		    if(randomDirection == 100) {
		    	randomValue = randomDirection + randomFloor;
		   } else {
			   randomValue = randomDirection + (55 - randomFloor);
		   }
		    elevatorPositionArray[i] = randomValue;
			
		}

	}
	
	
	
	//Zugangssimulation Stockwerke UP
	public int assignFloorNumber() {

	    return (int)(Math.random()*(max-min+1)+min);  

	}
	
	//Zugangssimulation Aufzüge UP 
	public int assignElevatorNumber(int randomFloor) {
		int floorNr = randomFloor % 7;
		if(floorNr == 0) {
			floorNr = 7;
		}
		return floorNr;

	}
		
}
