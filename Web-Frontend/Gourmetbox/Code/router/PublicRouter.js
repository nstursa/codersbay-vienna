const express = require('express');
const passport = require('passport');
const jwtService = require('../services/JwtService');
const router = express.Router();
const db = require('../services/DatabaseService');


router.post('/register', async (req, res) => {
    if(!req.body.name || !req.body.email || !req.body.password) {
        res.status(400).send('Name, e-mail address or password is missing');
        return;
    }
    try {
        await db.createUser(req.body);
        res.status(201).send('User created');
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/login', passport.authenticate('local', {session: false}), async (req, res) => {
    const jwtToken = jwtService.signUser(req.user);
    return res.send(jwtToken);
});

module.exports = router;
