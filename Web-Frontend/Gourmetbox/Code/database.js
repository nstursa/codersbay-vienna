const mysql = require('mysql2/promise');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv').config();

let connection;
mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_DB,
    password: process.env.DB_PW
}).then(con => {
    connection = con;
    console.log('Database connected');
}).catch(err => console.error(err));


//Alles aus datenbank warenkorb
async function getAllCountries() {
    const [result] = await connection.execute('SELECT * FROM warenkorb');
    return result;
}


//Alles sortier nach Preis
async function getCountriesByPopulation(min, max) {
    const [result] = await connection.execute('SELECT * FROM warenkorb WHERE Produkt > ? AND Preis < ?', [min, max]);
    return result;
}

module.exports = {
    getAllCountries,
    getCountriesByPopulation
};
