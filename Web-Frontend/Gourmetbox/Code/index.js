const express = require('express');
const db = require('./database');
const cors = require('cors');
const router = require('./router/PublicRouter');
const strategyJwt = require('./strategies/JwtStrategy');
const strategyLocal = require('./strategies/LocalStrategy');
const passport = require('passport');


//Nodemailer
const nodemailer = require("nodemailer");



//Passport & JWT
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static('public'));
passport.use("local",strategyLocal);
passport.use("jwt", strategyJwt);
app.use(passport.initialize());
app.use("/api",router);



//Kontaktform
app.get('/Kontaktform', (req, res)=>{
    res.sendFile(__dirname + '/public/Kontaktform.html')
}) 

app.post('/Kontaktform', (req, res)=>{
    console.log("Test");
    console.log(req.body);

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'testninoangelo@gmail.com',
            pass: 'Test1234#'

        }
    })


const mailOptions = {
    from: req.body.email,
    to: 'testninoangelo@gmail.com',
    subject: `Message from ${req.body.email}: ${req.body.subject}`,
    text: req.body.message
}
    transporter.sendMail(mailOptions, (error, info)=>{
        if(error){
            console.log(error);
            res.send('error');
        } else {
            console.log('Email versendet: '+ info.response);
            res.send('success')
        }

    })
})






//Warenkorb laden
app.get('/GMCart', async (req, res) => {
    try {
        const countries = await db.getAllCountries();
        res.json(countries);
    } catch (err) {
        res.status(500).send(err);
    }
});

app.get('/GMCart/:min/:max', async (req, res) => {
    try {
        const countries = await db.getCountriesByPopulation(req.params.min, req.params.max);
        res.json(countries);
    } catch (err) {
        res.status(500).send(err);
    }
});

app.listen(3000, err => {
    if(err) {
        console.error('cannot start webserver');
        console.error(err);
        return;
    }
    console.log('Webserver running on port 3000');
});
