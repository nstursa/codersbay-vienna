const loginForm = document.getElementById('loginForm');
const regForm = document.getElementById('RegForm');
const errAlert = document.getElementById('invalidCredentialsAlert');

loginForm.addEventListener('submit', async e => {

    console.log("Test");


    e.preventDefault();
    e.stopPropagation();

    if (!loginForm.checkValidity()) {
        return;
    }
    const email = document.getElementById('emailInput').value;
    const password = document.getElementById('passwordInput').value;

    const loginResponse = await fetch('/api/login', {
        method: 'POST',
        body: JSON.stringify({email, password}),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if(loginResponse.status !== 200) {
        errAlert.style.display = 'block';
        return;
    }
    const jwt = await loginResponse.text();
    localStorage.setItem('jwt', jwt);
    location.href = "user.html";
});

regForm.addEventListener('submit1', async e => {


    e.preventDefault();
    e.stopPropagation();

    if (!regForm.checkValidity()) {
        console.log("Test");
        return;
    }

    const name = document.getElementById('usaInput').value;
    const email = document.getElementById('emailInput1').value;
    const password = document.getElementById('passwordInput1').value;

    console.log(name, email, password);

    const regResponse = await fetch('/api/register', {
        method: 'POST',
        body: JSON.stringify({name, email, password}),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if(regResponse.status !== 201) {
        errAlert.style.display = 'block';
        return;
    }
    const jwt = await regResponse.text();
    localStorage.setItem('jwt', jwt);
    location.href = "user.html";
});
