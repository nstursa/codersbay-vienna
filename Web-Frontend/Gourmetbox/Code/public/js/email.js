const contactForm = document.querySelector('.contact-form');

let name = document.getElementById('name');
let email = document.getElementById('email');
let subject = document.getElementById('subject');
let message = document.getElementById('message');

//Auslesen der DATEN von Kontaktform
contactForm.addEventListener('submit', (e) => {
    e.preventDefault();

    let formData = {
        name: name.value,
        email: email.value,
        subject: subject.value,
        message: message.value
    }
    console.log(formData);

    //Alert EMAIL VERSENDET
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/Kontaktform');
    xhr.setRequestHeader('content-type', 'application/json');
    xhr.onload = function(){
        console.log('HIIIIIIIEEEER');
        console.log(xhr.responseText);
        if(xhr.responseText == 'success'){
            alert('Email versendet');
            name.value = '';
            email.value = '';
            subject.value = '';
            message.value = '';
        }
        else {
            alert('Email konnte nicht versendet werden!')
        }
    }

    xhr.send(JSON.stringify(formData));
    
})