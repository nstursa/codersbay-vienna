const countryTable = document.getElementById('countryTableBody');

const refreshBtn = document.getElementById('refresh');
refreshBtn.addEventListener('click', loadCountries);

async function loadCountries() {
    const countryResponse = await fetch('http://localhost:3000/GMCart');
    const countries = await countryResponse.json();

    countryTable.innerHTML = '';
    for (const country of countries) {
        appendCountryToTable(country);
    }
}

function appendCountryToTable(country) {
    const tr = document.createElement('tr');
    for(let key of ['ID', 'Produkt', 'Preis']) {
        const td = document.createElement('td');
        td.textContent = country[key];
        tr.appendChild(td);
    }

    const img = document.createElement("img");
    img.src = country.Image;
    img.classList.add('bilderWarenkorb')
    const imgTd = document.createElement("div");
    imgTd.appendChild(img);
    tr.appendChild(imgTd);
    countryTable.appendChild(tr);
}
