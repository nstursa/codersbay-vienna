window.addEventListener('load', async () => {
    const jwt = localStorage.getItem('jwt');
    if(!jwt) {
        location.href = 'GMWebsiteDieBoxen.html';
        return;
    }
    const userResponse = await fetch('/api/user', {
       headers: {
           Authorization: 'Bearer ' + jwt
       }
    });
    if(userResponse.status !== 200) {
        location.href = 'GMWebsiteDieBoxen.html';
        return;
    }
    const user = await userResponse.json();
    console.log(user);
    document.getElementById('title').textContent = 'Hallo ' + user.name;
});
