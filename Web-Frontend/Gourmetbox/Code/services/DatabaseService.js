const mysql = require('mysql2/promise');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv').config();

let connection;
mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_DB,
    password: process.env.DB_PW
}).then(con => {
    connection = con;
}).catch(console.error);


async function createUser({name, email, password}) {
    const passwordHash = await bcrypt.hash(password, 9);
    await connection.execute('INSERT INTO users (name, email, password) VALUES (?, ?, ?)', [name, email, passwordHash]);
}

async function findUserByCredentials({email, password}) {
    const [result] = await connection.execute('SELECT * FROM users WHERE email = ? LIMIT 1', [email]);
    if(!result.length) {
        throw new Error('No user with given email address found');
    }
    const user = result[0];
    const doPasswordsMatch = await bcrypt.compare(password, user.password);
    if(!doPasswordsMatch) {
        throw new Error('Passwords do not match');
    }
    delete user.password;
    return user;
}

module.exports = {
    createUser,
    findUserByCredentials
}
