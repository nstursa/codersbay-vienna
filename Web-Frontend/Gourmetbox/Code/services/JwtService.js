const jwt = require('jsonwebtoken');

const JWT_SECRET = 'mySuperSecretNobodyWillEverFindOut';
const JWT_EXPIRES_IN = '31d';

function signUser(user) {
    return jwt.sign({
        sub: user.userId,
        name: user.name,
        email: user.email
    }, JWT_SECRET, {expiresIn: JWT_EXPIRES_IN});
}

module.exports = {
    signUser,
    JWT_SECRET
}
