//TODO
const WebSocket = require('ws')
const express = require('express')
const path = require('path')
const { render } = require('ejs')
const port = 3000
const app = express();


app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

const wss = new WebSocket.Server({port: 8080})



wss.on('connection', ws => {
    ws.room = '';
    ws.on('message', message => {
        // websocketSendToAll(message);
        console.log(`Received message => ${message}`)
        let msg = JSON.parse(message)

        if (msg.joinRoom) {
            ws.room = msg.joinRoom
        }
        if (msg.room) {
            websocketSendToAll(message)
        }
    })
    ws.send(JSON.stringify({message: 'Hello! Message From Server!!'}))
})

function websocketSendToAll(text) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            if (client.room === JSON.parse(text).room) {
                client.send(text);
            }
        }
    });
}

app.get('/', function(req, res){
    res.render('chat');
})

app.listen(port, function(err){
    if (err) console.log(err);
    console.log("Server listening on PORT 3000", port);
});