let vm = new Vue({
  el: '#app',
  data: {
    details: 'Socks',
    birds: []
  },
  methods: {
    async showBirds(){
      var bird= await axios.get("http://localhost:3000/birds");
      this.birds = bird.data;
  },
  getBirdAuthors(bird){
    let output = '';
    for(let i in bird.attributes){
      output += bird.attributes[i].author + ', ';
    }
    return output;
  }
}
});
