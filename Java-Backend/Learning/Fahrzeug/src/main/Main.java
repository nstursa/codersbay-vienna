package main;

import java.util.*;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import Fahrzeuge.Fahrzeuge;
import Fahrzeuge.PKW;
import infrastruktur.Autohaus;



public class Main {
	
	
	
//// -------- AUSSEN VARIABLEN -----------	
	static int lkws = 2;
	static int pkws = 3;
	static int anzahl = lkws + pkws;
	static int verdoppeln = anzahl*2;
//	

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		
		
	    Scanner scanner = new Scanner(System.in);
	    String str[] = {"year", "month", "day" };
	    String date = "";

	    for(int i=0; i<3; i++) {
	        System.out.println("Enter " + str[i] + ": ");
	        date = date + scanner.next() + "/";
	    }
	    date = date.substring(0, date.length()-1);
	    System.out.println("date: "+ date); 

	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	    Date parsedDate = null;

	    try {
	        parsedDate = dateFormat.parse(date);
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }
	  
		
		
	
		
		
// --------------- NEUE FAHRZEUGE REGISTRIERUNG ----------------	
		
		Autohaus autohaus = new Autohaus(5, 8, false, "Wiesenthal");
		
		autohaus.addCar("PKW", 1, "Audi", 1, 1, 1);
		autohaus.addCar("PKW", 2, "Mercedes", 2, 2, 2);
		autohaus.addCar("PKW", 3, "VW", 3, 3, 3);
		
		autohaus.addCar("LKW", 4, "Volvo", 4, 4, 4);
		autohaus.addCar("LKW", 5, "VW", 5, 5, 5);
		
		boolean first = true;
		
		
		
		
		
		for(int i = 1; i <= autohaus.getAutoAnzahl(); i++) {
			System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl der Fahrzeuge: " + i);
			if (i < autohaus.getAutoAnzahl()) {
				System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
			}
			if (i == autohaus.getAutoAnzahl()) {
				for(Fahrzeuge fahrzeuge : autohaus.getFahrzeugListe()) {
					if (first) {
						System.out.print("Folgende Marken sind zu verkaufen: " + fahrzeuge.getMarke());
						first = false;
					} else {
						System.out.print(", " + fahrzeuge.getMarke());
					}
				}
			}
			
		}
		System.out.println();
		
		System.out.println("Das Autohaus ist überlastet: "+autohaus.zuVieleFahrzeuge());
		System.out.println("Das Autohaus ist überlastet: "+autohaus.zuVieleFahrzeuge(10));
		

		
		
		
		for (int i = 0; i < autohaus.getFahrzeugListe().size(); i++) {
			System.out.println("Der Preis des Fahrzeuges ist: "+ autohaus.getFahrzeugListe().get(i).getAutoPreis());
		}
		
		for(int i = 0; i < autohaus.getFahrzeugListe().size(); i++) {
		
		
		if (autohaus.getFahrzeugListe().get(i) instanceof PKW) {
			System.out.println("Das Fahrzeug ist ein PKW.");
		} else {
			System.out.println("Das Fahrzeug ist ein LKW.");
		}
		
		}
	

	}
	

}

