package Fahrzeuge;


public class LKW extends Fahrzeuge {

	

	// -------- VARIABLEN -----------
		double rabattProJahr = 6;
		String lkwName;
		double autoPreis;
		int alterFahrzeug;
		
		

		
		
		
	// -------- KONSTRUKTOR -----------	

		public LKW(int id, String marke, int baujahr, double autoPreis, String lkwName) {
			super(id, marke, autoPreis, baujahr);

			this.lkwName = lkwName;


		}

		

		


		

	// -------- RABATT -----------	
		
		@Override
		public double getRabatt() {

			double summe = alterFahrzeug * rabattProJahr;
			
			if(summe >= 15) {

				summe = 15;

			} 
			return summe;

		}

		

		

		

	// ---------------- GETPREIS ---------------	
		
		@Override
		public double getPreis() {

			double preis = autoPreis - (autoPreis / 100 * getRabatt());
			return preis;
		}



		

		

		

	// -------------- PRINT --------------
		
		@Override
		public void print() {

			System.out.println(lkwName+ " \n" + "Id:" + getId() + " \n" + "Marke: " + getMarke() + " \n" + "Das Baujahr: " + getBaujahr() + " \n" + "Fahrzeugalter: " + alterFahrzeug + " \n" + "Rabatt/Jahr: " + rabattProJahr + "%" + " \n" + "Autopreis: " + autoPreis );
		}
}



