package Fahrzeuge;


public abstract class Fahrzeuge {

	
	

	private int id;
	private String marke;
	private double autoPreis;
	private int baujahr;


	
	

	
	public Fahrzeuge(int id, String marke, double autoPreis, int baujahr) {
		super();
		this.id = id;
		this.marke = marke;
		this.autoPreis = autoPreis;
		this.baujahr = baujahr;
	}
	
	
	
	
	
	public abstract double getRabatt();
	public abstract double getPreis();
	public abstract void print();





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getMarke() {
		return marke;
	}





	public void setMarke(String marke) {
		this.marke = marke;
	}





	public double getAutoPreis() {
		return autoPreis;
	}





	public void setAutoPreis(double autoPreis) {
		this.autoPreis = autoPreis;
	}





	public int getBaujahr() {
		return baujahr;
	}





	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	
	
	
	
}
	