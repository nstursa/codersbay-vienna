package infrastruktur;

import Fahrzeuge.Fahrzeuge;
import Fahrzeuge.PKW;
import Fahrzeuge.LKW;

import java.util.*;



public class Autohaus {
	
// ----------------------------- OBJEKTE ERSTELLEN ----------------------------------
	
	private ArrayList<Fahrzeuge> fahrzeugListe = new ArrayList<Fahrzeuge>();
	
	
	
// -------- VARIABLEN -----------	

	public ArrayList<Fahrzeuge> getFahrzeugListe() {
		return fahrzeugListe;
	}



	public void setFahrzeugListe(ArrayList<Fahrzeuge> fahrzeugListe) {
		this.fahrzeugListe = fahrzeugListe;
	}




	private int mitarbeiter;
	private int autoAnzahl = fahrzeugListe.size();
	private int abstellplatz;
	private boolean auslastung;
	private String autohauString;
	
	


	
	
// -------- KONSTRUKTOR -----------
	
public Autohaus(int mitarbeiter, int abstellplatz, boolean auslastung, String autohauString) {
		this.mitarbeiter = mitarbeiter;
		this.abstellplatz = abstellplatz;
		this.auslastung = auslastung;
		this.autohauString = autohauString;
//		this.fahrzeugListe = new ArrayList<Fahrzeuge>;
	}



//-------- ZUVIELE FAHRZEUGE METHODE -------
public void addCar(String fahrzeugTyp, int id, String marke, int baujahr, double grundpreis, int serviceJahr ) {
	if(fahrzeugTyp.equals("PKW")) {
		PKW pkw = new PKW(fahrzeugTyp, id, marke, grundpreis, baujahr, serviceJahr);   // <----- muss gleich wie Konstruktor in pkw or lwk sein
		fahrzeugListe.add(pkw);
	} else if(fahrzeugTyp.equals("LKW")) {
		LKW lkw = new LKW(id, marke, baujahr, serviceJahr, marke);
		fahrzeugListe.add(lkw);
	} else {
		System.out.println("Der Typ des Fahrzeugs ist nicht korrekt.");
	}
}





// -------- ZUVIELE FAHRZEUGE METHODE ----------
	
	public boolean zuVieleFahrzeuge() {
		if(fahrzeugListe.size() > abstellplatz) {
			auslastung = true;
		} else {
			auslastung = false;
		}
		return auslastung;
	}
	
	
	
	
	public boolean zuVieleFahrzeuge(int mitarbeiter) {
		if(mitarbeiter*3 < fahrzeugListe.size()) {
			auslastung = true;
		} else {
			auslastung = false;
		}
		return auslastung;
	}


	
	
// -------- PRINT ----------
	
	public void print() {
		System.out.println("~ ~ ~ <AUTOHAUS NINO> ~ ~ ~");
	}
	
	

	
	
	
	
	
// -------- GETTER & SETTER -----------

	public int getMitarbeiter() {
		return mitarbeiter;
	}




	public void setMitarbeiter(int mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
	}




	public int getAutoAnzahl() {
		return fahrzeugListe.size();
	}




	public void setAutoAnzahl(int autoAnzahl) {
		this.autoAnzahl = autoAnzahl;
	}




	public int getAbstellplatz() {
		return abstellplatz;
	}




	public void setAbstellplatz(int abstellplatz) {
		this.abstellplatz = abstellplatz;
	}




	public boolean isAuslastung() {
		return auslastung;
	}




	public void setAuslastung(boolean auslastung) {
		this.auslastung = auslastung;
	}




	public String getAutohauString() {
		return autohauString;
	}




	public void setAutohauString(String autohauString) {
		this.autohauString = autohauString;
	}

	
	
	
	
	
	
	
	
}
