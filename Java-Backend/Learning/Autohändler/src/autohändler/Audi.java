package autoh�ndler;

public class Audi extends Auto {
	
	boolean quattro;
	
	public Audi(int ps, int t�rzahl, String farbe, boolean quattro) {
		super("Audi", ps, t�rzahl, farbe);
		this.quattro = quattro;
	}
	
	@Override
	public void starten() {
		super.starten();
		System.out.println("Ja und zwar ein Audi startet");
	}
}
